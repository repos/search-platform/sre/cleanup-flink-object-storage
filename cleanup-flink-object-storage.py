#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0
# cleanup-flink-object-storage.py:
# given a kubernetes flink job/release (static?),
# checks which objects can be safely removed and optionally removes them.
#
# Inspired by/stolen from export.py in the Puppet repo.

import boto3
import json
import kubernetes
import logging
import plac
import os
import yaml

from datetime import datetime, timedelta, timezone
from pathlib import Path
from pprint import pprint

logger = logging.getLogger(__name__)

# allow boto to validate certs with our custom bundle

os.environ['AWS_CA_BUNDLE'] = "/etc/ssl/certs/ca-certificates.crt"

def load_config():
    config_file = "{}/.objstore.yaml".format(Path.home())
    with open(config_file) as f:
        conf = yaml.safe_load(f)["config"]
    return conf

def find_current_job_id(env, release):
    '''Given a wikikube env and helm release, finds current job ID of running
    flinkdeployment.'''
    kubernetes.config.load_kube_config()
    custom_api = kubernetes.client.CustomObjectsApi()
# looking for the equivalent of `kubectl get flinkdeployments.flink.apache.org -l release=commons -o json | jq '.items[0].status.jobStatus.jobId' -r`
    custom_objects = custom_api.list_namespaced_custom_object(namespace='rdf-streaming-updater',
                     group='flink.apache.org', version='v1beta1', plural='flinkdeployments')
    custom_objs = (custom_objects['items'])
    for obj in custom_objs:
        if release == obj['metadata']['labels']['release']:
            current_job_id = obj['status']['jobStatus']['jobId']
            logger.info("Current job ID for release %s in env %s is %s",
                     release, env, current_job_id)
            return current_job_id
    raise ValueError("Could not find a job ID for release {} in env {}".format(release, env))

def login(conf):
    '''Login and create S3 session. '''
    endpoint_url = "https://{}".format(conf["endpoint"])
    access_key = conf["access_key"]
    secret_key = conf["secret_key"]
    s3 = boto3.session.Session()
    client = s3.client('s3', **{
        "endpoint_url": endpoint_url,
        "aws_access_key_id": access_key,
        "aws_secret_access_key": secret_key
    })
    return client

def get_objects_for_bucket(bucket, client):
    all_objects = []
    # use pagination to avoid the 1000-item limit of S3 API
    paginator = client.get_paginator('list_objects_v2')
    pagination_config = {
        'PageSize': 1000,
    }
    pages = paginator.paginate(Bucket=bucket, PaginationConfig=pagination_config)
    for page in pages:
        if 'Contents' in page:
            for obj in page['Contents']:
                all_objects.append(obj)
    return all_objects

def find_old_objects(all_objects, current_job_id, release):
    ''' Given the full list of objects, filter out all except those which match the
    release, are older than 30 days, and don't contain the current job ID in their
    metadata. '''
    gross_storage_used = 0
    net_storage_used = 0
    today = datetime.now()
    utc_today = today.replace(tzinfo=timezone.utc)
    thirty_days_ago = utc_today - timedelta(days=30)
    old_objs = []
    logger.info('Checking for objects older than 30 days for release %s', release)
    for obj in all_objects:
        gross_storage_used = gross_storage_used + int(obj['Size'])
        if "zookeeper_ha_storage" not in obj['Key'] and release in obj['Key'] and current_job_id not in obj['Key']:
            last_modified = datetime.fromisoformat(str(obj['LastModified']))
            # the below logic is correct, but looks wrong in natural language.
            if last_modified < thirty_days_ago:
                old_objs.append(obj['Key'])
                net_storage_used = net_storage_used + int(obj['Size'])
#     pct_storage_used = (net_storage_used / gross_storage_used)
#     print (gross_storage_used, net_storage_used)
#     print ("{} percentage of total storage used belongs to objects older than"
#             " 30 days".format(pct_storage_used))
    return gross_storage_used, net_storage_used, old_objs

def delete_objects(bucket, client, old_objs):
# FIXME: S3 API only allows 1000 objects per delete request. Not a big deal for our
# use case, but we should probably support > 1000 objects at some point.
    objs_to_delete = []
    for obj in old_objs:
        objs_to_delete.append({'Key': obj })
    print ("Deleting the following objects:\n")
    pprint(old_objs)
    del_request = client.delete_objects(
        Bucket=bucket,
        Delete={
        'Objects': objs_to_delete,
        'Quiet': False}
    )

@plac.annotations(
env=plac.Annotation("deployment env to use", choices=['codfw', 'eqiad', 'staging']),
release=plac.Annotation("helm release to use", choices=['commons', 'wikidata'])
)
def main(env, release):
    conf = load_config()
    client = login(conf)
    current_job_id = find_current_job_id(env, release)
    bucket = "rdf-streaming-updater-{}".format(env)
    all_objects = get_objects_for_bucket(bucket, client)
    gross_storage_used, net_storage_used, old_objs = find_old_objects(all_objects, current_job_id, release)
    logger.info("Release '%s' has total of %s objects older than 30 days"
                "using %s bytes in %s bucket",
                    release, len(old_objs), gross_storage_used, bucket)
    report = input("Release '{}' has a total of {} objects older than 30 days using "
               "{} bytes in {} bucket.\nDelete? [Y/N] "
               .format(release, len(old_objs), gross_storage_used, bucket))
    print (report)
    if report == "Y":
        delete_objects(bucket, client, old_objs)

if __name__ == '__main__':
    plac.call(main)
