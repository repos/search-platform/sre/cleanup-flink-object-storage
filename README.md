# cleanup-flink-object-storage

 cleanup-flink-object-storage.py:
 given a kubernetes flink job/release (static?),
 checks which objects can be safely removed and optionally removes them.
 Inspired by/stolen from [export.py in the Puppet repo](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/puppet/+/refs/heads/production/modules/thanos/files/bucket-query/export.py) .


##  Building a dev environment (requires SRE-level access for pkg installation. Reach out for help!)
- Use a deployment server (such as deploy2002), as it has the secrets you need to access object storage and kubernetes.
- Clone the cleanup script to your homedir
- Install prereq python libraries via apt-get: python3-boto3 (for S3 libraries) and python3-plac (CLI parser).
- Create .objstore.yaml in your homedir based on the objstore.yaml in this repo
- Verify that you can run the script (as is, it just does a simple read-only command).

## License
Apache 2.0
